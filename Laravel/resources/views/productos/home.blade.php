@extends('layouts.app')

@section('title','Prueba App')

@section('content')



    <div class="alert alert-danger fade in" style="display:none;">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Registro NO Eliminado!</strong> El registro NO se ha eliminado, ocurrio un error en el servidor.
    </div>

    <div class="alert alert-success fade in" style="display:none;">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Registro Eliminado!</strong> El registro se ha eliminado con éxito.
    </div>


      <table id="productos" class="table display" style="width:100%; border: 1px solid #ddd;">
        <thead>
            <tr>
                <th>SKU</th>
                <th>Descripcion</th>
                <th>Modelo</th>
                <th>Marca</th>
                <th>Categoría</th>
                <th>Stock</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody></tbody>
      </table>

      <script>
      $(document).ready(function() {
        var token = '{{ csrf_token() }}';

        $('#productos').DataTable({
          "language": {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando _START_ al _END_ registros de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":    "Último",
                "sNext":    "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
          },
          "order": [[ 0, "desc" ]],
          "processing": true, //Feature control the processing indicator.
          "info": true,
          "scrollX": true, // enables horizontal scrolling
          "filter": true,
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          "aLengthMenu": [[10, 25, 50, 100, 150, 200, 500, 900], [10, 25, 50, 100, 150, 200, 500, 900]],
          "ajax" : {
            'url':"{{ route('ajaxdata') }}",
            'type':"POST",
            'data': { '_token': token }
          },
          aoColumns: [
            { data:"sku"},
            { data:"descripcion"},
            { data:"modelo"},
            { data:"marca"},
            { data:"categoria"},
            { data:"sku"},
            {
              data: null,
              defaultContent: '<div class="action"></div> ',
              orderable: false,
              searchable: false
            },
          ],
          "fnCreatedRow" : function(nRow, oData, iDataIndex) {
            var id = $("td:eq(0)", nRow).html();
            var buttons = "";

            var marca = $("td:eq(3)", nRow).html();
            $("td:eq(3)", nRow).html( '<span class="label label-success">'+marca+'</span>' );

            var categoria = $("td:eq(4)", nRow).html();
            $("td:eq(4)", nRow).html( '<span class="label label-warning">'+categoria+'</span>' );

            $.ajax({
               url: "/obtenerStock",
               type: "POST",
               data: { '_token': token, 'id' : id },
               dataType: 'json',
               cache: false,
               success: function( data ) {
                 var stock_pcel = parseInt(data.stock[0].stock_pcel);
                 var stock_prov_local = parseInt(data.stock[0].stock_prov_local);
                 var stock_prov_foraneo = parseInt(data.stock[0].stock_prov_foraneo);

                 var $total = ( stock_pcel + stock_prov_local + stock_prov_foraneo );

                 $("td:eq(5)", nRow).html( $total - parseInt(data.stock[0].comprometido ) );
               },
               error: function( xhr, status, errorThrown ){
                 console.log( "STATUS CODE: " + xhr.status + "RESPONSE: " + xhr.responseText );
               }
            });


    				buttons += '<a href="/editar/'+id+'" class="btn btn-primary methodCallAjax" title="Editar Clientes"> Editar </a>';
            buttons += ' ';
            buttons += '<a href="javascript:void(0);" class="btn btn-danger" onClick="javascript:deleteRecord('+id+')"> Eliminar </a>';

            $("td:eq(6)", nRow).html('<div style="width:150px;">'+buttons+'</div>');
          },

          "fnInitComplete": function(oSettings, json) {}
        });
      });

      function deleteRecord( id ) {
        var token = '{{ csrf_token() }}';

        $.ajax({
           url: "/delete",
           type: "POST",
           data: {sku : id},
           dataType: 'json',
           cache: false,
           beforeSend: function (request) {
             return request.setRequestHeader('X-CSRF-Token', token );
           },
           success: function( data ) {
             $(".alert-success").show();
             $(".alert-success").delay(2000).fadeOut("slow", function () { $(this).css('display','none'); });
           },
           error: function( xhr, status, errorThrown ){
             $(".alert-danger").show();
             $(".alert-danger").delay(2000).fadeOut("slow", function () { $(this).css('display','none'); });
           }
        });

        $('#productos').DataTable().ajax.reload();
  		}

      </script>

@endsection
