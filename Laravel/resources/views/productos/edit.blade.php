@extends('layouts.app')

@section('title','Prueba App')

@section('content')

<link rel="stylesheet" href="http://alxlit.name/bootstrap-chosen/bootstrap.css">

  <div class="row" style="border: 1px solid #ddd;padding:30px;">
    <div class="alert alert-success fade in" style="display:none;">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Registro Actualizado!</strong> El registro se ha Actualizado con éxito.
    </div>

    <div class="alert alert-danger fade in" style="display:none;">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Error al Insertar Registro!</strong> Ah Ocurrido un Problema al Insertar ese registro, posiblemente se encuentre duplicado, revise sus datos.
    </div>

    <form class="form-horizontal" id="edit_recordset">
      <div><center><h3> EDITAR PRODUCTO </h3></center></div>

      @foreach($productos as $producto)
      <div class="form-group form-group-sm">
        <label for="sku" class="col-xs-3 control-label">Código ^SKU</label>
        <div class="col-xs-9">
          <input type="text" class="form-control" id="sku" name="sku" value="{{ $producto->sku }}">
        </div>
      </div>

      <div class="form-group form-group-sm">
        <label for="descripcion" class="col-xs-3 control-label">Descripción</label>
        <div class="col-xs-9">
          <input type="text" class="form-control" id="descripcion" name="descripcion" value="{{ $producto->descripcion }}">
        </div>
      </div>

      <div class="form-group form-group-sm">
        <label for="modelo" class="col-xs-3 control-label">Modelo</label>
        <div class="col-xs-9">
          <input type="text" class="form-control" id="modelo" name="modelo" value="{{ $producto->modelo }}">
        </div>
      </div>

      <div class="form-group form-group-sm">
        <label for="marca" class="col-xs-3 control-label">Marca</label>
        <div class="col-xs-9">
          <select class="form-control chosen" name="marca">
            @foreach($marcas as $item)
              @if( $producto->marca == $item->marca)
                <option value="{{$item->marca}}" selected>{{$item->descripcion}}</option>
              @else
                <option value="{{$item->marca}}">{{$item->descripcion}}</option>
              @endif
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group form-group-sm">
        <label for="categoria" class="col-xs-3 control-label">Categoría</label>
        <div class="col-xs-9">
          <select class="form-control chosen" name="categoria">
            @foreach($categorias as $item)
              @if( $producto->categoria == $item->categoria)
                <option value="{{$item->categoria}}" selected>{{$item->descripcion}}</option>
              @else
                <option value="{{$item->categoria}}">{{$item->descripcion}}</option>
              @endif
            @endforeach
          </select>
        </div>
      </div>

      @endforeach

      @foreach($stocks as $sto)

      <div class="form-group form-group-sm">
        <label for="stock_pcel" class="col-xs-3 control-label">Stock en Tienda PCEL</label>
        <div class="col-xs-9">
          <input type="text" class="form-control" id="stock_pcel" name="stock_pcel" value="{{$sto->stock_pcel}}">
        </div>
      </div>

      <div class="form-group form-group-sm">
        <label for="stock_plocal" class="col-xs-3 control-label">Stock Proveedor Local</label>
        <div class="col-xs-9">
          <input type="text" class="form-control" id="stock_plocal" name="stock_plocal" value="{{$sto->stock_prov_local}}">
        </div>
      </div>

      <div class="form-group form-group-sm">
        <label for="stock_pforaneo" class="col-xs-3 control-label">Stock Proveedor Foraneo</label>
        <div class="col-xs-9">
          <input type="text" class="form-control" id="stock_pforaneo" name="stock_pforaneo" value="{{$sto->stock_prov_foraneo}}">
        </div>
      </div>

      <div class="form-group form-group-sm">
        <label for="comprometido" class="col-xs-3 control-label">Stock Comprometido</label>
        <div class="col-xs-9">
          <input type="text" class="form-control" id="comprometido" name="comprometido" value="{{$sto->comprometido}}">
        </div>
      </div>

      @endforeach


      <center>
        <input type="submit" class="btn btn-primary" value="Guardar Registro">
      </center>

    </form>

  </div>

<script type="text/javascript">
  var token = '{{ csrf_token() }}';

  var registro = $('#edit_recordset').validate({
    rules : {
      sku : {
        required : true
      },
      descripcion : {
        required : true
      },
      modelo : {
        required : true
      },
      stock_pcel : {
        required : true
      },
      stock_plocal : {
        required : true
      },
      stock_pforaneo : {
        required : true
      },
      comprometido : {
        required : true
      }
    },
    // Messages for form validation
    messages : {
      sku : {
        required : 'Se requiere escribir un Código SKU de Producto'
      },
      descripcion : {
        required : 'Se requiere escribir una descripción del Producto'
      },
      modelo : {
        required : 'Se requiere escribir un modelo de Producto'
      },
      stock_pcel : {
        required : 'Se requiere escribir un número Stock'
      },
      stock_plocal : {
        required : 'Se requiere escribir un Stock para el Proveedor Local'
      },
      stock_pforaneo : {
        required : 'Se requiere seleccionar un Stock para Proveedor Foraneo'
      },
      comprometido : {
        required : 'Se requiere escribir un Stock Comprometido'
      }
    },
    highlight: function(element) {
      $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
      $(element).closest('.form-group').removeClass('has-error');
    },
    submitHandler: function(form, event) {
      event.stopPropagation(); // Stop stuff happening
      event.preventDefault(); // Totally stop stuff happening

      var _form = $('#edit_recordset');
      var formData = _form.serialize();

      $.ajax({
         url: "/edit",
         type: "POST",
         data: formData,
         dataType: 'json',
         cache: false,
         beforeSend: function (request) {
           return request.setRequestHeader('X-CSRF-Token', token );
         },
         success: function( data ) {
           $(".alert-success").show();
           $(".alert-success").delay(2000).fadeOut("slow", function () { $(this).css('display','none'); });
         },
         error: function( xhr, status, errorThrown ){
           $(".alert-danger").show();
           $(".alert-danger").delay(2000).fadeOut("slow", function () { $(this).css('display','none'); });
         }
      });
    }

  });

</script>

@endsection
