CREATE TABLE marcas (
 marca smallint(6) NOT NULL,
 descripcion varchar(45) COLLATE latin1_general_ci NOT NULL,
 baja datetime DEFAULT NULL,
 PRIMARY KEY (marca)
);

CREATE TABLE `categorias` (
 `categoria` smallint(6) NOT NULL,
 `descripcion` varchar(45) COLLATE latin1_general_ci NOT NULL,
 `baja` datetime DEFAULT NULL,
 PRIMARY KEY (categoria)
)

CREATE TABLE `prods_stock` (
 `sku` int(11) unsigned NOT NULL,
 `stock_pcel` int(11) NOT NULL DEFAULT '0',
 `stock_prov_local` int(11) NOT NULL DEFAULT '0',
 `stock_prov_foraneo` int(11) NOT NULL DEFAULT '0',
 `comprometido` int(11) NOT NULL DEFAULT '0',
 PRIMARY KEY (sku)
)

CREATE TABLE `productos` (
 sku int(11) NOT NULL,
 descripcion varchar(255) COLLATE latin1_general_ci NOT NULL,
 modelo varchar(45) COLLATE latin1_general_ci NOT NULL,
 marca smallint(6) NOT NULL,
 categoria smallint(6) NOT NULL,
 baja datetime DEFAULT NULL,
 PRIMARY KEY (sku),
 KEY `xif1productos` (`modelo`),
 KEY `xif2productos` (`marca`),
 KEY `xif3productos` (`categoria`),
 CONSTRAINT `productos_ibfk_1` FOREIGN KEY (`marca`) REFERENCES marcas (`marca`),
 CONSTRAINT `productos_ibfk_2` FOREIGN KEY (`categoria`) REFERENCES `categorias` (`categoria`)
)






-- falta imagen




insert into marcas values (1,'EPSON',null);
insert into marcas values (2,'HP',null);
insert into marcas values (3,'IBM',null);
insert into marcas values (4,'KINGSTON',null);
insert into marcas values (5,'LG',null);
insert into marcas values (6,'TOSHIBA',null);
insert into marcas values (7,'XEROX',null);
insert into marcas values (8,'APPLE',null);
insert into marcas values (9,'KENSINGTON',null);
insert into marcas values (10,'ZEBRA',null);
insert into marcas values (11,'CYBERPOWER',null);
insert into marcas values (12,'STARTECH.COM',null);
insert into marcas values (13,'TECH PAD',null);
insert into marcas values (14,'POLYCOM',null);
insert into marcas values (15,'LEVYDAL',null);

insert into categorias values (2,'Accesorios',null);
insert into categorias values (3,'Almacenamiento',null);
insert into categorias values (6,'Computadoras',null);
insert into categorias values (9,'Electrónica',null);
insert into categorias values (12,'Imagen e Impresión',null);


insert into productos values (185876,'DISCO EXTERNO ','HDTC930XW3CA',6,3,1);
insert into productos values (185882,'TABLET TECH PAD 13.3" ','PAD TECHPAD K13',13,6,1);
insert into productos values (185924,'APUNTADOR LASSER 4 EN 1','P3605',9,2,1);
insert into productos values (185936,'CABLE PARA CAJON DE DINERO','42M5657',3,12,1);
insert into productos values (185970,'BRAZO SOPORTE ARTICULADO','ARMSLIM',12,2,1);
insert into productos values (185978,'CINTA EPSON AZUL','LK-4WLN',1,12,1);
insert into productos values (185986,'IMPRESORA','GX43-102510-000',10,12,1);
insert into productos values (185992,'NOBREAK CYBERPOWER','PR3000LCDSL',11,9,1);
insert into productos values (186008,'PROYECTOR','V11H726520',1,9,1);
insert into productos values (186028,'BANDEJA XEROX','497K13630',7,12,1);
insert into productos values (186032,'USB 16 GB RANA','Rana silicón',15,3,1);
insert into productos values (186176,'PLOTTER HP T830','F9A30A',2,12,1);
insert into productos values (186190,'FUNDA APPLE PARA IPAD','STM-222-160JW-01',8,2,1);
insert into productos values (186208,'MEMORIA KING','DTIG3/8GBZ',4,3,1);
insert into productos values (186214,'TABLET ','SIM700',13,6,1);
insert into productos values (186216,'MONITOR','22MK430H-B',5,9,1);
insert into productos values (186246,'CAMARA DE VIDEO','2200-46200-025',14,9,1);

insert into prods_stock values (185876,15,180,0,3);
insert into prods_stock values (185882,0,15,2,1);
insert into prods_stock values (185924,81,302,52,7);
insert into prods_stock values (185936,6,3,0,0);
insert into prods_stock values (185970,13,42,31,0);
insert into prods_stock values (185978,0,0,11,3);
insert into prods_stock values (185986,7,49,52,6);
insert into prods_stock values (185992,1,6,9,0);
insert into prods_stock values (186008,71,78,0,20);
insert into prods_stock values (186028,3,7,16,1);
insert into prods_stock values (186032,9,0,8,3);
insert into prods_stock values (186176,81,15,0,10);
insert into prods_stock values (186190,54,70,12,16);
insert into prods_stock values (186208,7,47,34,5);
insert into prods_stock values (186214,31,5,10,1);
insert into prods_stock values (186216,12,8,16,20);
insert into prods_stock values (186246,2,6,10,1);
