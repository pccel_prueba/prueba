<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


/*
|--------------------------------------------------------------------------
| Datatable
|--------------------------------------------------------------------------
*/
// Route to show a Datatable
Route::get('/','ProductoController@index');

// Data Source to Get Datatable
Route::post('datasource','ProductoController@datasource')->name('ajaxdata');

// Get Stoks via Ajax
Route::post('obtenerStock','ProductoController@stocks');


/*
|--------------------------------------------------------------------------
| new Record
|--------------------------------------------------------------------------
*/

// Route to create a new Record and Save
Route::get('nuevo','ProductoController@create');
Route::post('new', 'ProductoController@store');

/*
|--------------------------------------------------------------------------
| update Record
|--------------------------------------------------------------------------
*/

// Route to edit a Record Existing and Save
Route::get('editar/{id}','ProductoController@edit');
Route::post('edit','ProductoController@update');

/*
|--------------------------------------------------------------------------
| Delete Record
|--------------------------------------------------------------------------
*/
Route::post('delete', 'ProductoController@destroy');
