<?php

namespace App\Http\Controllers;

use DB;
use View;
use DataTables;
use DateTime;
use App\Producto;
use Illuminate\Http\Request;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('productos.home');
    }

    /**
     * Display a data source.
     *
     * @return \Illuminate\Http\Response
     */
     public function datasource()
     {
        // $productos = Producto::select('sku','descripcion','modelo','marca','categoria')->join('marcas','marca','=','marca')->join('categorias','categoria','=','categoria');
        $productos = DB::table('productos')
        ->join('categorias', 'productos.categoria', '=', 'categorias.categoria')
        ->join('marcas', 'productos.marca', '=', 'marcas.marca')
        ->select(['productos.sku as sku', 'productos.descripcion as descripcion', 'productos.modelo as modelo', 'marcas.descripcion as marca', 'categorias.descripcion as categoria', 'productos.baja as bajas'])
        ->where('productos.baja',"=","0000-00-00 00:00:00")
        ->orWhere('productos.baja', "=", NULL);

        return DataTables::of($productos)
                ->make(true);
     }

    /**
     * Generator of Stocks beetwen the SKU
     *
     * @return \Illuminate\Http\Response
     */
     public function stocks(Request $request)
     {
       $sku        = $request->input('id');
       $stocks     = DB::table('prods_stock')
                      ->where('sku',$sku)
                      ->get()
                      ->toArray();

       return [ 'stock' => $stocks ];
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $marcas     = DB::table('marcas')->get()->toArray();
        $categorias = DB::table('categorias')->get()->toArray();

        return View::make('productos.create')
                ->with('marcas', $marcas)
                ->with('categorias', $categorias);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $sku        = $id;
      $producto   = DB::table('productos')->where('sku', $sku)->get();
      $stock      = DB::table('prods_stock')->where('sku', $sku)->get();

      $marcas     = DB::table('marcas')->get()->toArray();
      $categorias = DB::table('categorias')->get()->toArray();

      return view('productos.edit')
              ->with('productos', $producto )
              ->with('stocks', $stock)
              ->with('marcas', $marcas)
              ->with('categorias', $categorias);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $sku                = $request->input('sku');
      $descripcion        = $request->input('descripcion');
      $modelo             = $request->input('modelo');
      $marca              = $request->input('marca');
      $categoria          = $request->input('categoria');

      $stock_pcel         = $request->input('stock_pcel');
      $stock_prov_local   = $request->input('stock_plocal');
      $stock_prov_foraneo = $request->input('stock_pforaneo');

      $baja               = NULL;

      $comprometido       = 0;

      $productos  = DB::select( 'call new_product(?,?,?,?,?,?)', array($sku, $descripcion, $modelo, $marca, $categoria, $baja) );
      $stocks     = DB::select( 'call new_stock(?,?,?,?,?)', array($sku, $stock_pcel, $stock_prov_local, $stock_prov_foraneo, $comprometido) );

      $response = array( 'status' => 'success' );

      return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $sku                = $request->input('sku');
      $descripcion        = $request->input('descripcion');
      $modelo             = $request->input('modelo');
      $marca              = $request->input('marca');
      $categoria          = $request->input('categoria');

      $stock_pcel         = $request->input('stock_pcel');
      $stock_prov_local   = $request->input('stock_plocal');
      $stock_prov_foraneo = $request->input('stock_pforaneo');
      $comprometido       = $request->input('comprometido');
      
      $productos  = DB::select( 'call update_product(?,?,?,?,?)', array($sku, $descripcion, $modelo, $marca, $categoria) );
      $stocks     = DB::select( 'call update_stock(?,?,?,?,?)', array($sku, $stock_pcel, $stock_prov_local, $stock_prov_foraneo, $comprometido) );

      $response = array( 'status' => 'success' );

      return response()->json($response);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $sku                = $request->input('sku');
      $current_date       = new DateTime();
      $deleteDate         = $current_date->format('Y-m-d H:i:s');

      $productos  = DB::select('call delete_product(?,?)', array($sku, $deleteDate) );

      $response = array( 'status' => 'success' );

      return response()->json($response);

    }
}
